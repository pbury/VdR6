# Tables de référence

Ce dossier contient toutes les tables de références liées à l'article 6 de la rubrique *Vie de la recherche* dans les revues *Savoirs* et *Transformations*. Tous ces fichiers sont disponibles sous licence **CC-BY-SA**.

Les fichiers sont :
- Pour le noyau dur :
    - *noyau_2010_2020.xslx* : l'ensemble des thèses du noyau dur pour la période 2010 à avril 2020.
    - *noyau_2018-2019_full.xlsx* : Les thèses du noyau dur pour la période 2018 à 2020
- Pour le deuxième cercle :
    - *deuxieme_cercle_2010_2020.xslx* : Les thèse du deuxième cercle (celles avec un score d'aurité de jury supérieur à 0.5 et au moins une catégorie avec une distance sémantique au noyau supérieurs à 0.9) pour la période 2010 à avril 2020
    - *deuxieme_cercle_2018_2019_full.xslx* : Les thèse du deuxième cercle (celles avec un score d'aurité de jury supérieur à 0.5 et au moins une catégorie avec une distance sémantique au noyau supérieurs à 0.9) pour la période 2018 à avril 2020.
    - *deuxieme_cercle_full.xlsx* : l'ensemble des thèses du deuxième cercle sans aucun filtrage. L'utilisateur a le loisir de place ses propres seuils sur les différentes colonnes.
- Pour les jurés :
    - *scores_jures.xslx* : Les scores de spécificité FdA des personnes liées aux thèses.
    