import configparser
from pymongo import MongoClient
import rdflib
from rdflib import URIRef, Graph
import re
from datetime import datetime, date
from html.parser import HTMLParser

from Marc21 import Marc21


class Common:
    config = None
    collection_noyau_theses = None

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('noyau_dur.ini')

        self.mongo_client = MongoClient(
            self.config['mongo']['host'],
            int(self.config['mongo']['port'])
        )

        self.mongo_db = self.mongo_client[self.config['mongo']['noyau_db']]
        self.collection_noyau_theses = self.mongo_db[self.config['mongo']['noyau_theses']]
        self.collection_noyau_people = self.mongo_db[self.config['mongo']['noyau_people']]
        self.collection_productions = self.mongo_db[self.config['mongo']['productions']]

        self.lemmatized_phrases = None
        self.lemmatized_phrases = (
            "éducation adulte",
            "formation adulte",
            "éducation permanent",
            "formation long vie",
            "validation acquis",
            "vae",
            "didactique professionnel",
            "échange savoir",
            "apprenance"
        )
        self.lemmatized_exclusions = None
        self.lemmatized_exclusions = {
            "enseigner"
        }
        self.marc21code = Marc21()

    def is_URIRef(self, data):
        if type(data) == rdflib.term.URIRef:
            return True
        return False

    def get_predicate_name(self, predicate):
        predicates = {
            URIRef('http://xmlns.com/foaf/0.1/name'): 'Name',
            URIRef('http://purl.org/dc/elements/1.1/subject'): 'Subject',
            URIRef('http://purl.org/dc/terms/subject'): 'Subject',
            URIRef('http://purl.org/dc/terms/creator'): 'Creator',
            URIRef('http://purl.org/dc/terms/contributor'): 'Contributor',
            URIRef('http://purl.org/dc/terms/modified'): 'ModifiedDate',
            URIRef('http://purl.org/dc/terms/created'): 'CreatedDate',
            URIRef('http://www.loc.gov/loc.terms/relators/ths'): 'Thesis',
            URIRef('http://purl.org/ontology/bibo/Thesis'): 'Thesis',
            URIRef('http://purl.org/dc/terms/abstract'): 'Abstract',
            URIRef('http://purl.org/ontology/bibo/Document'): 'Document',
            URIRef('http://xmlns.com/foaf/0.1/Person'): 'Person',
            URIRef('http://purl.org/dc/elements/1.1/title'): 'Title',
            URIRef('http://purl.org/dc/elements/1.1/date'): 'Date',
            URIRef('http://purl.org/dc/terms/created'): 'CreationDate',
            URIRef('http://purl.org/dc/terms/dateAccepted'): 'AcceptedDate',
            URIRef('http://xmlns.com/foaf/0.1/Organization'): 'Organization',
            URIRef('http://www.loc.gov/loc.terms/relators/'): 'Relators',
            URIRef('http://www.loc.gov/loc.terms/relators/dgg'): 'Relators DGG',
            URIRef('http://www.loc.gov/loc.terms/relators/auth'): 'Relators Auth'
        }
        if predicate in predicates:
            return predicates[predicate]
        else:
            return 'relation'

    def is_literal(self, data):
        if type(data) == rdflib.term.Literal:
            return True
        return False

    def process_xml(self, doc):
        # we send author's ppn for next step
        data = {}
        graph = Graph()
        graph.parse(data=doc)

        predicates = graph.predicates(subject=None, object=None)
        for predicate in predicates:
            for s, p, o in graph.triples((None, predicate, None)):
                source = s.toPython().strip('<').strip('>').split(' ')[0]
                if re.match('^http', source):
                    if source not in data:
                        data[source] = {}
                    if self.is_URIRef(o):
                        objet = o.toPython().strip('<').strip('>').split(' ')[0]
                        predicate_name = self.get_predicate_name(o)
                        if 'type' not in data[source]:
                            data[source]['type'] = predicate_name
                        if predicate_name == 'relation':
                            if 'related' not in data[source]:
                                data[source]['related'] = []
                            if objet not in data[source]['related']:
                                data[source]['related'].append(objet)
                    else:
                        predicate_name = self.get_predicate_name(p)
                        if predicate_name in ['CreationDate', 'ModifiedDate', 'AcceptedDate']:
                            if type(o.value) == datetime:
                                data[source][predicate_name] = o.value
                            elif type(o.value) == date:
                                dt = datetime.combine(o.value, datetime.min.time())
                                data[source][predicate_name] = dt
                            elif type(o.value) == str:
                                data[source][predicate_name] = datetime.strptime(o.value, '%Y-%m-%d')
                            else:
                                print(predicate_name, o.value, type(o.value))
                        elif predicate_name in ['Name', 'Date']:
                            if predicate_name not in data[source]:
                                data[source][predicate_name] = []
                            data[source][predicate_name].append(o.value)
                        else:
                            if predicate_name not in data[source]:
                                data[source][predicate_name] = []
                            if self.is_URIRef(o) and objet not in data[source][predicate_name]:
                                data[source][predicate_name].append(objet)
                            language = ''
                            if hasattr(o, 'language') and o.language is not None:
                                language = o.language
                            try:
                                value = o.value
                                if (language, value) not in data[source][predicate_name] and self.is_literal(o):
                                    data[source][predicate_name].append((language, value))
                            except Exception as e:
                                pass
        return data

    def count_noyau_dur(self, doc, theses_noyau_dur):
        if doc['id'] in theses_noyau_dur:
            return 1
        else:
            return 0

    def count_garants(self, role, theses_noyau_dur):
        nb = 0
        if role['unimarcCode'] in self.marc21code.marc21.garantsmarc21CodeList:
            nb_roles = int(role['count'])
            if nb_roles == 1:
                nb += self.count_noyau_dur(role['doc'], theses_noyau_dur)
            else:
                for these in role['doc']:
                    nb += self.count_noyau_dur(these, theses_noyau_dur)
        return nb

    def update_categorie(self, collection, sub_cat, cat):
        collection.update_many(
            {sub_cat: 1},
            {
                '$set': {
                    cat: True,
                }
            }
        )
        collection.update_many(
            {sub_cat: 1},
            {
                '$addToSet': {
                    "categories": cat
                }
            }
        )

    def is_role_a_garants_one(self, role):
        if role['marc21Code'] in self.marc21code.garantsmarc21Code:
            return True
        else:
            return False

    def is_role_in_these_jury_one(self, role):
        if role['marc21Code'] in self.marc21code.thesesmarc21code:
            return True
        else:
            return False

    def extract_thesis(self, role, people_id, thesis_noyau_dur, thesis_initital_query):
        for document in role['doc']:
            if type(document) == dict and 'these' in document['referentiel']:
                noyau_dur = False
                if document['id'] in thesis_noyau_dur:
                    noyau_dur = True
                initial_query = False
                if document['id'] in thesis_initital_query:
                    initial_query = True
                self.collection_productions.update_one(
                    {"_id": document['id']},
                    {
                        '$set': {
                            "citation": document['citation'],
                            'noyau_dur': noyau_dur,
                            'initial_query': initial_query,
                            'processing_step': 11
                        }
                    },
                    upsert=True
                )
                self.collection_productions.update_one(
                    {"_id": document['id']},
                    {
                        '$addToSet': {
                            'garants': people_id
                        }
                    }
                )
                self.collection_productions.update_one(
                    {"_id": document['id']},
                    {
                        '$unset': {
                            'nb_garant': True
                        }
                    }
                )


class MyHTMLParser(HTMLParser):

    def __init__(self, common, thesis_id):
        super().__init__()
        self.common = common
        self.thesis_id = thesis_id

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            dcterms_contributor = False
            for attr in attrs:
                if attr[0] == 'rel':
                    dcterms_contributor = True
            if dcterms_contributor:
                for attr in attrs:
                    if attr[0] == 'resource':
                        resource = attr[1].replace('http://www.idref.fr/', '').replace('/id', '')
                        res = self.common.collection_noyau_people.find(
                            {'_id': resource},
                            {'is_garant': True}
                        )
                        is_garant = False
                        if res is not None and 'is_garant' in res and res['is_garant'] is True:
                            is_garant = True
                        self.common.collection_productions.update_one(
                            {'_id': self.thesis_id},
                            {
                                '$addToSet': {
                                    'ressources': resource
                                }
                            }
                        )

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        pass