import xlsxwriter

from Common import Common

common = Common()

workbook = xlsxwriter.Workbook('output_data/deuxieme_cercle_full.xlsx')
worksheet = workbook.add_worksheet()

columns = {
    'DFP': 'G',
    'EDM': 'H',
    'IOX': 'I',
    'LAN': 'K',
    'POA': 'L',
    'SCO': 'M',
    'UJI': 'N',

}
columns_valeurs = {
    'DFP': 'O',
    'EDM': 'P',
    'IOX': 'Q',
    'LAN': 'R',
    'POA': 'S',
    'SCO': 'T',
    'UJI': 'U',

}
worksheet.write('A1', 'These ID')
worksheet.write('B1', 'Year')
worksheet.write('C1', 'Auteur')
worksheet.write('D1', 'Directeur de thèse')
worksheet.write('E1', 'Titre')
worksheet.write('F1', 'Résumé')
worksheet.write(columns['DFP'] + '1', 'DFP')
worksheet.write(columns['EDM'] + '1', 'EDM')
worksheet.write(columns['IOX'] + '1', 'IOX')
worksheet.write(columns['LAN'] + '1', 'LAN')
worksheet.write(columns['POA'] + '1', 'POA')
worksheet.write(columns['SCO'] + '1', 'SCO')
worksheet.write(columns['UJI'] + '1', 'UJI')
worksheet.write(columns_valeurs['DFP'] + '1', 'DFP')
worksheet.write(columns_valeurs['EDM'] + '1', 'EDM')
worksheet.write(columns_valeurs['IOX'] + '1', 'IOX')
worksheet.write(columns_valeurs['LAN'] + '1', 'LAN')
worksheet.write(columns_valeurs['POA'] + '1', 'POA')
worksheet.write(columns_valeurs['SCO'] + '1', 'SCO')
worksheet.write(columns_valeurs['UJI'] + '1', 'UJI')

common = Common()
i = 1
for doc in common.collection_productions.find({'$and': [{'is_soutenue': True}, {'score_autorite_pondere':{'$gte':0.5}}]}):
    i += 1
    print(doc['_id'])
    worksheet.write('A' + str(i), doc['_id'])
    worksheet.write('B' + str(i), doc['Year'])
    if 'author_name' in doc:
        worksheet.write('C' + str(i), doc['author_name'])
    worksheet.write('E' + str(i), doc['Title'])
    worksheet.write('F' + str(i), doc['Abstract'])
    if 'directeur_name' in doc:
        worksheet.write('D' + str(i), doc['directeur_name'])
    for column in columns:
        category = 'prediction_' + column
        if category in doc and doc[category] > 0.9:
            worksheet.write(columns[column] + str(i), 1)
        worksheet.write(columns_valeurs[column] + str(i), int(100*doc[category]))

workbook.close()



