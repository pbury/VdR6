import re
from Common import Common

"""
Exclu certaines thèses basées sur un fichier d'exclusion (établi heutistiquement)
"""
common = Common()

excluded_ids = []
with open('input_data/excluded.txt', 'r') as fh:
    for line in fh.readlines():
        excluded_ids.append(line.strip())
excluded_ids = list(set(excluded_ids))

for doc in common.collection_noyau_theses.find({'processing_step': 5}):
    print(doc['_id'])
    ok = False
    for phrase in common.lemmatized_exclusions:
        phrase_excluded = False
        exclusion = {
            'Abstract_lemmatized': False,
            'Title_lemmatized': False,
            'Subjects_lemmatized': False
        }
        if 'Abstract_lemmatized' in doc and re.match('.*' + phrase + '.*', doc['Abstract_lemmatized']):
            phrase_excluded = True
            ok = True
        if 'Title_lemmatized' in doc and re.match('.*' + phrase + '.*', doc['Title_lemmatized']):
            phrase_excluded = True
            ok = True
        if 'Subjects_lemmatized' in doc and re.match('.*' + phrase + '.*', doc['Subjects_lemmatized']):
            ok = True
            phrase_excluded = True

        common.collection_noyau_theses.update_one(
            {"_id": doc['_id']},
            {
                '$set': {
                    'exclusion_' + phrase.replace(' ', '_'): phrase_excluded
                }
            }
        )
        common.collection_noyau_theses.update_one(
            {"_id": doc['_id']},
            {
                '$unset': {
                    'exclusion_abstract_' + phrase.replace(' ', '_'): True,
                    'exclusion_title_' + phrase.replace(' ', '_'): True,
                    'exclusion_subject_' + phrase.replace(' ', '_'): True,
                }
            }
        )
    common.collection_noyau_theses.update_one(
        {"_id": doc['_id']},
        {
            '$set': {
                'excluded': ok,
                'processing_step': 6
            }
        }
    )

for thesis_id in excluded_ids:
    common.collection_noyau_theses.update_one(
        {"thesis_id": thesis_id},
        {
            '$set': {
                'excluded': True,
                'included': False,
                'processing_step': 6
            }
        }
    )

