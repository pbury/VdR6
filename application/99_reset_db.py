import spacy
import re
from TextProcessing import TextProcessing
from Common import Common

common = Common()

common.collection_noyau_theses.update_many(
    {},
    {
        '$unset': {
            'Abstract': True,
            'Subjects': True,
            'Title': True,
            'Year': True,
            'Abstract_bow': True,
            'Abstract_lemmatized': True,
            'Title_bow': True,
            'Title_lemmatized': True,
            'Subjects_bow': True,
            'Subjects_lemmatized': True,
            'included': True,
            'inclusion_éducation_adulte': True,
            'inclusion_formation_adulte': True,
            'inclusion_éducation_permanent': True,
            'inclusion_formation_long_vie': True,
            'inclusion_validation_acquis': True,
            'inclusion_vae': True,
            'inclusion_didactique_professionnel': True,
            'inclusion_échange_savoir': True,
            'inclusion_apprenance': True,
            'exclusion_enseigner': True,
            'excluded': True,
            'noyau_dur': True,
            'ressources': True,
            'people_directeur': True,
            'people_rapporteur': True,
            'people_president': True,
            'people_jures': True,
            'jures': True,
            'nb_jures': True,
        }
    }
)

common.collection_noyau_theses.update_many(
    {},
    {
        '$set': {
            'processing_step': 2
        }
    }
)