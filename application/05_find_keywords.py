import spacy
import re
from TextProcessing import TextProcessing
from Common import Common

"""
Cherche les theses dont les textes contiennent les mots recherchés cote à cote après lemmatisation
Les phrases recherchées sont :
    "éducation adulte",
    "formation adulte",
    "éducation permanent",
    "formation long vie",
    "validation acquis",
    "vae",
    "didactique professionnel",
    "échange savoir",
    "apprenance"
    Cela positionne le champ *included*
"""

common = Common()
phrases = (
    "éducation des adultes",
    "formation des adultes",
    "éducation permanente",
    "formation tout au long de la vie",
    "validation des acquis",
    "VAE",
    "didactique professionnelle",
    "échange de savoirs",
    "échanges de savoirs",
    "apprenance"
)

if common.lemmatized_phrases is None:
    # import spacy and update stopwords
    nlp = spacy.load("fr_core_news_md")
    TextProcessing.update_spacy_stopword(
        nlp,
        ("",),
        ("",),
    )
    processor = TextProcessing(
        nlp,
        ("--t::z",)
    )
    for phrase in phrases:
        processor.clear_lemmatized_text()
        processor.set_text(phrase)
        processor.preprocess_one_text()
        processor.tokenize_and_lemnatize()
        lemmatized_text = processor.get_lemmatized_text()
        print(phrase, " ".join(lemmatized_text))

for doc in common.collection_noyau_theses.find({'processing_step': 4}):
    print(doc['thesis_id'])
    ok = False
    for phrase in common.lemmatized_phrases:
        phrase_ok = False
        inclusion = {
            'Abstract_lemmatized': False,
            'Title_lemmatized': False,
            'Subjects_lemmatized': False
        }
        if 'Abstract_lemmatized' in doc and re.match('.*' + phrase + '.*', doc['Abstract_lemmatized']):
            inclusion['Abstract_lemmatized'] = True
            ok = True
            phrase_ok = True
        if 'Title_lemmatized' in doc and re.match('.*' + phrase + '.*', doc['Title_lemmatized']):
            inclusion['Title_lemmatized'] = True
            phrase_ok = True
            ok = True
        if 'Subjects_lemmatized' in doc and re.match('.*' + phrase + '.*', doc['Subjects_lemmatized']):
            inclusion['Subjects_lemmatized'] = True
            phrase_ok = True
            ok = True
        common.collection_noyau_theses.update_one(
            {"_id": doc['_id']},
            {
                '$set': {
                    'included': ok,
                    'inclusion_' + phrase.replace(' ', '_'): phrase_ok ,
                }
            }
        )
        common.collection_noyau_theses.update_one(
            {"_id": doc['_id']},
            {
                '$unset': {
                    'inclusion_abstract_' + phrase.replace(' ', '_'): True,
                    'inclusion_title_' + phrase.replace(' ', '_'): True,
                    'inclusion_subject_' + phrase.replace(' ', '_'): True,

                }
            }
        )
    common.collection_noyau_theses.update_one(
        {"_id": doc['_id']},
        {
            '$set': {
                'included': ok,
                'processing_step': 5
            }
        }
    )

