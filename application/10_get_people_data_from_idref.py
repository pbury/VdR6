import requests

from Common import Common

"""
Pour chaque personne trouvée à l'étape précedente, on télécharge sa notice d'autorité à partir d'IdRef
"""
common = Common()

processed_ids = []
i = 0
for doc in common.collection_noyau_people.find({'data': {'$exists': False}}):
    i += 1
    doc_id = doc['_id']
    print(i, doc_id)
    if doc_id not in processed_ids:
        processed_ids.append(doc_id)
        uri = 'https://www.idref.fr/PpnReferences?format=text/json&sudocOnly=false&param=' + doc_id
        print(uri)
        response = requests.get(uri)
        try:
            result = response.json()

            common.collection_noyau_people.update_one(
                {'_id': doc_id},
                {
                    '$set': {
                        'data': result,
                        'processing_step': 10
                    }
                },
                upsert=True
            )
        except:
            print("excep")
            pass

