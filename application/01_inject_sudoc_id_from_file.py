import csv
from Common import Common

"""
Lit la liste des identifiants de thèse retournée par les requetes initales et les insère en BDD

"""


def main(filename):

    common = Common()

    with open(filename, newline='') as csvfile:
        file_reader = csv.reader(csvfile, delimiter=';')
        for row in file_reader:
            sudoc_id = row[0]
            print(sudoc_id)
            common.collection_noyau_theses.update_one(
                {'_id': sudoc_id},
                {
                    '$set': {
                        '_id': sudoc_id,
                        'processing_step': 1
                    }
                },
                upsert=True
            )


if __name__ == '__main__':
    main('input_data/uniq_sudoc_ids.txt')
