import xlsxwriter

from Common import Common

common = Common()

columns = {
    'DFP': 'C',
    'EDM': 'D',
    'IOX': 'E',
    'LAN': 'F',
    'POA': 'G',
    'SCO': 'H',
    'UJI': 'I'

}
lines = {
    'DFP': 3,
    'EDM': 4,
    'IOX': 5,
    'LAN': 6,
    'POA': 7,
    'SCO': 8,
    'UJI': 9
}


seuil_proba = 0.9
workbook = xlsxwriter.Workbook('output_data/matrice_confusion_9.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A3', 'Prédiction')
worksheet.write('C1', 'test')
for column in columns:
    worksheet.write(columns[column] + '2', column)
for line in lines:
    worksheet.write('B' + str(lines[line]), line)

test_counts = {}
for column in columns:
    test_counts[column] = {}
    for line in columns:
        test_counts[column][line] = 0

common = Common()
i = 1
for doc in common.collection_productions.find({'test_set': True}):
    i += 1
    print(doc['_id'])
    for column in columns:
        prediction_name = 'prediction_' + column
        for line in lines:
            test_name = 'test_value_' + line
            if doc[prediction_name] >= seuil_proba and doc[test_name] == 1:
                test_counts[column][line] += 1

for column in columns:
    for line in lines:
        worksheet.write(columns[column] + str(lines[line]), test_counts[column][line])

workbook.close()



