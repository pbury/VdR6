from Common import Common
from openpyxl import load_workbook

"""
importe le jeu de test
"""

common = Common()
wb = load_workbook(filename='input_data/test_set.xlsx')
sheet_ranges = wb['Sheet1']
common.collection_productions.update_many(
        {},
        {
            '$set': {
                'test_set': False
            }
        }
    )
for i in range(2, 102):
    test_value = {
        'SCO': 0,
        'UJI': 0,
        'DFP': 0,
        'POA': 0,
        'LAN': 0,
        'EDM': 0,
        'IOX': 0
    }
    test_result = sheet_ranges['B' + str(i)].value
    if test_result in test_value:
        test_value[test_result] = 1
    test_result = sheet_ranges['C' + str(i)].value
    if test_result in test_value:
        test_value[test_result] = 1
    test_result = sheet_ranges['D' + str(i)].value
    if test_result in test_value:
        test_value[test_result] = 1
    common.collection_productions.update_one(
        {'_id': sheet_ranges['A' + str(i)].value},
        {
            '$set': {
                'test_set': True,
                'test_value_SCO': test_value['SCO'],
                'test_value_UJI': test_value['UJI'],
                'test_value_DFP': test_value['DFP'],
                'test_value_POA': test_value['POA'],
                'test_value_LAN': test_value['LAN'],
                'test_value_EDM': test_value['EDM'],
                'test_value_IOX': test_value['IOX']
            }
        }
    )
    if i % 100 == 0:
        print(i)
