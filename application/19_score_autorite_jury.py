from Common import Common

"""
Calcule le score d'autorite d'un jury à partir des scores de ses membres
"""
common = Common()

score_people = {}
liste_garants = []
for people in common.collection_noyau_people.find({'is_garant': True}):
    if 'score_autorite' in people:
        score_people[people['_id']] = people['score_autorite']
    else:
        score_people[people['_id']] = 0
    liste_garants.append(people['_id'])
    liste_garants = list(set(liste_garants))

for doc in common.collection_noyau_theses.find({'noyau_dur': True}):
    print(doc['_id'])
    nb_jures = doc['nb_jures']
    total_score = 0
    nb_garants = 0
    for people in doc['jures']:
        if people in score_people:
            total_score += score_people[people]
        if people in liste_garants:
            nb_garants += 1

    common.collection_noyau_theses.update_one(
        {'_id': doc['_id']},
        {
            '$set': {
                'score_autorite_brut': total_score,
                'score_autorite_pondere': total_score/nb_jures,
                'nb_jures_garants': nb_garants
            }
        }
    )

