import xlsxwriter

from Common import Common

common = Common()

workbook = xlsxwriter.Workbook('output_data/theses_du_noyau.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'These ID')
worksheet.write('B1', 'Auteur')
worksheet.write('C1', 'Titre')
worksheet.write('D1', 'Directeur de thèse')
worksheet.write('E1', 'Catégories')
worksheet.write('F1', 'Sous-catégories')
worksheet.write('G1', 'Année')


common = Common()
i = 1
for doc in common.collection_noyau_theses.find({'included': True}):
    i += 1
    print(doc['thesis_id'])
    worksheet.write('A' + str(i), doc['thesis_id'])
    if 'author_name' in doc:
        worksheet.write('B' + str(i), doc['author_name'])
    worksheet.write('C' + str(i), doc['Title'])
    if 'people_directeur' in doc:
        res = common.collection_noyau_people.find_one({'_id':doc['people_directeur']})
        worksheet.write('D' + str(i), res['name'])
    if 'categories' in doc:
        worksheet.write('E' + str(i), ",".join(doc['categories']))
    if 'sub_categories' in doc:
        worksheet.write('F' + str(i), doc['sub_categories'])
    if 'Year' in doc:
        worksheet.write('G' + str(i), doc['Year'])

workbook.close()



