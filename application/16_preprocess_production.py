import spacy
from TextProcessing import TextProcessing

from Common import Common

"""
Pour les champs mot-clefs, titre et résumé des thèse produites applique la tokenization, suppression des stop-words et la lemmatisation
"""

nlp = spacy.load("fr_core_news_md")
TextProcessing.update_spacy_stopword(
    nlp,
    ("",),
    ("",),
)
common = Common()

for doc in common.collection_productions.find({'processing_step': 15}):
    print(doc['_id'])
    processor = TextProcessing(
        nlp,
        ("--t::z",)
    )
    processor.clear_lemmatized_text()
    processor.set_text(doc['Abstract'])
    processor.preprocess_one_text()
    processor.tokenize_and_lemnatize()
    lemmatized_text = processor.get_lemmatized_text()
    common.collection_productions.update_one(
        {'_id': doc['_id']},
        {
            '$set': {
                'Abstract_lemmatized': " ".join(lemmatized_text),
                'Abstract_bow': lemmatized_text,
                'processing_step': 16
            }
        }
    )

    processor.clear_lemmatized_text()
    processor.set_text(doc['Title'])
    processor.preprocess_one_text()
    processor.tokenize_and_lemnatize()
    lemmatized_text = processor.get_lemmatized_text()
    common.collection_productions.update_one(
        {'_id': doc['_id']},
        {
            '$set': {
                'Title_lemmatized': " ".join(lemmatized_text),
                'Title_bow': lemmatized_text,
                'processing_step': 16
            }
        }
    )
    processor.clear_lemmatized_text()
    processor.set_text(doc['Subjects'])
    processor.preprocess_one_text()
    processor.tokenize_and_lemnatize()
    lemmatized_text = processor.get_lemmatized_text()
    common.collection_productions.update_one(
        {'_id': doc['_id']},
        {
            '$set': {
                'Subjects_lemmatized': " ".join(lemmatized_text),
                'Subjects_bow': lemmatized_text,
                'processing_step': 16
            }
        }
    )

