import requests
from Common import Common

"""
Recupère le contenu xml des thèses de la table production
"""
common = Common()

i = 0
for doc in common.collection_productions.find({'$and': [{'is_soutenue': True}, {'noyau_dur': False}]}):
    i += 1
    thesis_id = doc['_id']
    print('Acquisition # %s (%s)' % (i, thesis_id))
    uri = 'https://www.theses.fr/' + thesis_id + '.xml'
    response = requests.get(uri)
    xml = ''
    if response.status_code == 200:
        xml = response.content
    else:
        print('Bad status code for xml')
    common.collection_productions.update_one(
        {'_id': thesis_id},
        {
            '$set': {
                'xml': xml,
                'processing_step': 14
            }
        },
        upsert=True
    )
