from Common import Common

"""
Analyse le contenu xml des thèses
Extrait :
- le sujet
- le titre
- le résumé
- l'année de soutenance
"""

common = Common()
for thesis in common.collection_noyau_theses.find({'processing_step': 2}):
    print(thesis['_id'])
    if 'xml' in thesis:
        doc = thesis['xml']
        data = common.process_xml(doc)

        for entry in data:
            if 'type' in data[entry]:
                if data[entry]['type'] == 'Thesis':
                    subjects = []
                    for (lang, subject) in data[entry]['Subject']:
                        if lang == 'fr' or lang == '':
                            subjects.append(subject)
                    good_abstract = ''
                    for (lang, abstract) in data[entry]['Abstract']:
                        if lang == 'fr' or lang == '':
                            good_abstract = abstract
                    good_title = ''
                    for (lang, title) in data[entry]['Title']:
                        if lang == 'fr' or lang == '':
                            good_title = title
                    common.collection_noyau_theses.update_one(
                        {'thesis_id': thesis['thesis_id']},
                        {
                            '$set': {
                                'Title': good_title,
                                'Abstract': good_abstract,
                                'Subjects': ", ".join(subjects),
                                'Year': data[entry]['Date'][0],
                                'processing_step': 3
                            }
                        }
                    )

