import xlsxwriter

from Common import Common

common = Common()

ontos = ('IOX',
         'EDM',
         'SCO',
         'UJI',
         'DFP',
         'POA',
         'LAN',
         )
for onto in ontos:
    workbook = xlsxwriter.Workbook('output_data/theses_' + onto + '.xlsx')
    worksheet = workbook.add_worksheet()

    worksheet.write('A1', 'These ID')
    worksheet.write('B1', 'Annee')
    worksheet.write('C1', 'Ontologie')
    worksheet.write('D1', 'Texte')


    common = Common()
    i = 1
    for doc in common.collection_noyau_theses.find({'noyau_dur': True}):
        i += 1
        txt = doc['Title_lemmatized'] + doc['Abstract_lemmatized'] + doc['Subjects_lemmatized']
        print(doc['thesis_id'])
        worksheet.write('A' + str(i), doc['thesis_id'])
        worksheet.write('B' + str(i), doc['Year'])
        worksheet.write('C' + str(i), doc[onto])
        worksheet.write('D' + str(i), txt)

    workbook.close()



