import re
import unidecode


class TextProcessing:
    text = ''
    lemmatized_text = []
    bag_of_lemna = []
    nlp = None
    special_string_list = None

    def __init__(self, nlp, special_string_list):
        self.special_string_list = special_string_list
        self.nlp = nlp

    def set_text(self, text):
        self.text = text

    def get_text(self):
        return self.text

    def get_lemmatized_text(self):
        return self.lemmatized_text

    def clear_lemmatized_text(self,):
        self.lemmatized_text = []

    def remove_extra_spaces(self):
        self.text = re.sub(r'[\s]+', ' ', self.text)

    def convert_accents_to_ascii(self):
        self.text = unidecode.unidecode(self.text)

    def expand_contractions(self):
        pass

    def remove_special_chars(self):
        pass

    def lowercase_sentences(self):
        self.text = self.text.lower()

    def remove_numbers(self):
        self.text = re.sub(r'\d+', '', self.text)

    def remove_special_strings(self):
        for string_to_replace in self.special_string_list:
            self.text = self.text.replace(string_to_replace, "")

    def preprocess_one_text(self):
        self.remove_extra_spaces()
        self.expand_contractions()
        self.remove_special_chars()
        self.lowercase_sentences()
        self.remove_numbers()
        self.remove_special_strings()
        self.remove_extra_spaces()

    def tokenize_and_lemnatize(self):
        doc = self.nlp(self.text)
        for sentence in doc.sents:
            fr_sentence = self.nlp(sentence.text)
            for token in fr_sentence:
                if not token.is_space and not token.is_punct and not token.is_stop and len(token.lemma_) > 1:
                    self.lemmatized_text.append(token.lemma_)

    @staticmethod
    def update_spacy_stopword(nlp, deselect_stopwords, added_stopwords):
        for w in deselect_stopwords:
            if w in nlp.vocab:
                nlp.vocab[w].is_stop = False
        for w in added_stopwords:
            nlp.vocab[w].is_stop = True
