import re
from Common import Common

"""
Extraction des noms de personnes
"""
common = Common()
regex = re.compile(r"\(\d\d.*", re.IGNORECASE)
for res in common.collection_noyau_people.find({'processing_step': 10}):
    name = res['data']['sudoc']['result']['name']
    id = res['_id']
    print(name)
    new_name = re.sub(r'\(\d\d.*', '', name)
    print(new_name.strip())
    common.collection_noyau_people.update_one(
        {'_id': id},
        {
            '$set': {
                'name': new_name,
                'processing_step': 11
            }
        }
    )