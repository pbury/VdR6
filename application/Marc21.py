
class Marc21:
    garantsmarc21Code = {}
    garantsmarc21CodeList = []
    thesesmarc21code = {}
    thesesmarc21codeList = []
    peoplemarc21code = {}
    peoplemarc21codeList = []

    def _set_garantsmarc21code(self):
        self.garantsmarc21Code = {
            'ths': {
                'full_name': "Directeur de thèse",
                'code': "727"
            },
            'Directeur de thèse': {
                'full_name': "Directeur de thèse",
                'code': "727"
            },
            'Rapporteur de la thèse': {
                'full_name': "Rapporteur de la thèse",
                'code': "958"
            }
        }

        for code in self.garantsmarc21Code:
            self.garantsmarc21CodeList.append(self.garantsmarc21Code[code]['code'])

    def _set_thesesmarc21code(self):
        self.thesesmarc21code = self.garantsmarc21Code.copy()
        self.thesesmarc21code['opn'] = {
                'full_name': "Membre du jury",
                'code': "555"
            }
        self.thesesmarc21code['Membre du jury'] = {
                'full_name': "Membre du jury",
                'code': "555"
            }
        self.thesesmarc21code['Président du jury de soutenance'] = {
            'full_name': "Président du jury de soutenance",
            'code': "956"
        }
        self.thesesmarc21codeList = self.garantsmarc21CodeList.copy()
        for code in self.thesesmarc21code:
            self.thesesmarc21codeList.append(self.thesesmarc21code[code]['code'])

    def _set_peoplemarc21code(self):
        self.peoplemarc21code = self.thesesmarc21code.copy()
        self.peoplemarc21code['aut'] = {
                'full_name': "Auteur",
                'code': "070"
            }
        self.peoplemarc21code['aui'] = {
                'full_name': "Préfacier, etc.",
                'code': "080"
            }
        self.peoplemarc21code['clb'] = {
                'full_name': "Collaborateur",
                'code': "xxx"
            }
        self.peoplemarc21code['edt'] = {
                'full_name': "Editeur scientifique",
                'code': "340"
            }
        self.peoplemarc21code['pbd'] = {
                'full_name': "Directeur de publication",
                'code': "651"
            }
        self.peoplemarc21code['rth'] = {
                'full_name': "Responsable de l'équipe de recherche",
                'code': '673'
            }
        self.peoplemarc21code['Personne interviewée'] = {
            'full_name': "Personne interviewée",
            'code': "460"
        }
        self.peoplemarc21code['drt'] = {
            'full_name': 'Metteur en scène',
            'code': '300'
        }
        self.peoplemarc21code['aft'] = {
            'full_name': "Auteur de la postface, du colophon, etc.",
            'code': "075"
        }
        self.peoplemarc21code['sec'] = {
            'full_name': "Secrétaire",
            'code': "710"
        }
        self.peoplemarc21code['trl'] = {
            'full_name': "Traducteur",
            'code': "730"
        }
        self.peoplemarc21code['sad'] = {
            'full_name': " Conseiller scientifique",
            'code': "695"
        }
        self.peoplemarc21code['ilu'] = {
            'full_name': "Enlumineur",
            'code': "430"
        }
        self.peoplemarc21code['ive'] = {
            'full_name': "Personne interviewée",
            'code': "460"
        }
        self.peoplemarc21code['spn'] = {
            'full_name': "Parraineur",
            'code': "723"
        }
        self.peoplemarc21code['res'] = {
            'full_name': "Directeur de la recherche",
            'code': "595"
        }
        self.peoplemarc21code['ccp'] = {
            'full_name': "Concepteur",
            'code': "245"
        }
        self.peoplemarc21code['ivr'] = {
            'full_name': "Interviewer",
            'code': "470"
        }
        self.peoplemarc21code['Interviewer'] = {
            'full_name': "Interviewer",
            'code': "470"
        }
        self.peoplemarc21code['com'] = {
            'full_name': "Compilateur",
            'code': "230"
        }
        self.peoplemarc21code['fmo'] = {
            'full_name': "Ancien possesseur",
            'code': "390"
        }
        self.peoplemarc21code['aus'] = {
            'full_name': "Dialoguiste",
            'code': "090"
        }
        self.peoplemarc21code['adp'] = {
            'full_name': "Adaptateur",
            'code': "010"
        }
        self.peoplemarc21code['csp'] = {
            'full_name': "Consultant de projet",
            'code': "255"
        }
        self.peoplemarc21code['grt'] = {
            'full_name': "Technicien graphique",
            'code': "410"
        }
        self.peoplemarc21code['pht'] = {
            'full_name': "Photographe",
            'code': "600"
        }
        self.peoplemarc21code['art'] = {
            'full_name': "Artiste",
            'code': "040"
        }
        self.peoplemarc21code['prt'] = {
            'full_name': "Artiste",
            'code': "040"
        }
        self.peoplemarc21code['pro'] = {
            'full_name': "Producteur",
            'code': "630"
        }
        self.peoplemarc21code['ill'] = {
            'full_name': "Illustrateur",
            'code': "440"
        }
        self.peoplemarc21code['cwt'] = {
            'full_name': "Auteur du commentaire",
            'code': "212"
        }
        self.peoplemarc21code['cmm'] = {
            'full_name': "Commentateur",
            'code': "210"
        }
        self.peoplemarc21code['rtm'] = {
            'full_name': "Membre de l'équipe de recherche",
            'code': "677"
        }
        self.peoplemarc21code['nrt'] = {
            'full_name': "Narrateur",
            'code': "550"
        }
        self.peoplemarc21code['org'] = {
            'full_name': "Instigateur",
            'code': "560"
        }
        self.peoplemarc21code['ins'] = {
            'full_name': "Auteur de l'envoi",
            'code': "450"
        }
        self.peoplemarc21code['dte'] = {
            'full_name': "Dédicataire",
            'code': "280"
        }
        self.peoplemarc21code['dnr'] = {
            'full_name': "Donateur",
            'code': "320"
        }
        self.peoplemarc21code['hnr'] = {
            'full_name': "Personne honorée",
            'code': "420"
        }
        self.peoplemarc21code['Commanditaire'] = {
            'full_name': "Commanditaire",
            'code': "COM"
        }
        self.peoplemarc21code['Producteur du fonds ou collectionneur'] = {
            'full_name': "Producteur du fonds ou collectionneur",
            'code': "PRO"
        }
        self.peoplemarc21code['rcp'] = {
            'full_name': "Destinataire de lettres",
            'code': "660"
        }
        self.peoplemarc21code['ctg'] = {
            'full_name': "Cartographe",
            'code': "180"
        }

        self.peoplemarc21codeList = self.thesesmarc21codeList.copy()
        for code in self.peoplemarc21code:
            self.peoplemarc21codeList.append(self.peoplemarc21code[code]['code'])

    def _set_full_marc21code(self):
        self.full_marc21code = self.peoplemarc21code.copy()

        self.full_marc21code['Fonction non précisée'] = {
            'full_name': 'Fonction non précisée',
            'code': 'xxx'
        }
        self.full_marc21code['dgg'] = {
            'full_name': "Organisme de soutenance",
            'code': "295"
        }
        self.full_marc21code['Organisme de soutenance'] = {
            'full_name': "Organisme de soutenance",
            'code': "295"
        }

        self.full_marc21code['Ecole doctorale associée à la thèse'] = {
            'full_name': "Ecole doctorale associée à la thèse",
            'code': "996"
        }
        self.full_marc21code['Laboratoire associé a la thèse'] = {
            'full_name': "Laboratoire associé a la thèse",
            'code': "981"
        }
        self.full_marc21code['orm'] = {
            'full_name': "Organisateur de réunion",
            'code': "557"
        }
        self.full_marc21code['000'] = {
            'full_name': "Collectivité éditrice",
            'code': "475"
        }
        self.full_marc21code['Equipe de recherche associée a la thèse'] = {
            'full_name': "Equipe de recherche associée a la thèse",
            'code': "984"
        }
        self.full_marc21code['Autre partenaire associé a la thèse'] = {
            'full_name': "Autre partenaire associé a la thèse",
            'code': ""
        }
        self.full_marc21code['Organisme de cotutelle'] = {
            'full_name': "Organisme de cotutelle",
            'code': "995"
        }
        self.full_marc21code['pbl'] = {
            'full_name': "Editeur commercial",
            'code': "650"
        }
        self.full_marc21code['Fondation associée a la thèse'] = {
            'full_name': "Fondation associée a la thèse",
            'code': "983"
        }
        self.full_marc21code['ant'] = {
            'full_name': "Antécédent bibliographique",
            'code': "100"
        }

        self.full_marc21code['Fonction non précisée'] = {
            'full_name': "Fonction non précisée",
            'code': "999"
        }
        self.full_marc21code['Autres'] = {
            'full_name': "Autres",
            'code': "AUT"
        }
        self.full_marc21code['oth'] = {
            'full_name': "Autres",
            'code': "570"
        }
        self.full_marc21code['Sujet'] = {
            'full_name': "Sujet",
            'code': "990"
        }

        self.full_marc21code['dst'] = {
            'full_name': "Distributeur",
            'code': "310"
        }
        self.full_marc21codeList = self.peoplemarc21codeList.copy()
        for code in self.full_marc21code:
            self.full_marc21codeList.append(self.full_marc21code[code]['code'])

    def __init__(self):
        self._set_garantsmarc21code()
        self._set_thesesmarc21code()
        self._set_peoplemarc21code()
        self._set_full_marc21code()

    @classmethod
    def check_marc21codes(cls, collection):
        for res in collection.find({}):
            results = res['data']['data']['sudoc']['result']
            if 'role' in results:
                for publis in results['role']:
                    if type(publis) == dict:
                        if publis['marc21Code'] not in cls.full_marc21code and publis['marc21Code'] != '':
                            print('missing ', publis['marc21Code'], publis['roleName'], publis['unimarcCode'])
        print('Marc21code Checked')
