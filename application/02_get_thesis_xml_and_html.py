import requests
from Common import Common

"""
Récupère le contenu html et xml des thèses analysées
"""
common = Common()

nb_processed = 0
for thesis in common.collection_noyau_theses.find({'thesis_id': {'$exists': True}}):
    thesis_id = thesis['thesis_id']
    nb_processed += 1
    print("processing %s : %s " % (nb_processed, thesis_id))

    uri = 'https://www.theses.fr/' + thesis_id + '.xml'
    response = requests.get(uri)
    xml = ''
    if response.status_code == 200:
        xml = response.content
    else:
        print('Bad status code for xml')

    uri = 'https://www.theses.fr/' + thesis_id
    response = requests.get(uri)

    html = ''
    if response.status_code == 200:
        html = response.content
    else:
        print('Bad status code for html')

    common.collection_noyau_theses.update_one(
        {'thesis_id': thesis_id},
        {
            '$set': {
                'xml': xml,
                'html': html,
                'processing_step': 2
            }
        },
        upsert=True
    )

