from rdflib import Graph
from xml.sax._exceptions import *
import re
from datetime import date
from datetime import datetime

from Common import Common

"""
Extrait les Années de publication, Titres, mot-clefs et résumés des thèses
"""

common = Common()
for thesis in common.collection_productions.find({'processing_step': 14}):
    doc = thesis['xml']
    print(thesis['_id'])

    # we send author's ppn for next step
    data = {}
    graph = Graph()
    try:
        graph.parse(data=doc)

        predicates = graph.predicates(subject=None, object=None)
        for predicate in predicates:
            for s, p, o in graph.triples((None, predicate, None)):
                source = s.toPython().strip('<').strip('>').split(' ')[0]
                if re.match('^http', source):
                    if source not in data:
                        data[source] = {}
                    if common.is_URIRef(o):
                        objet = o.toPython().strip('<').strip('>').split(' ')[0]
                        predicate_name = common.get_predicate_name(o)
                        if 'type' not in data[source]:
                            data[source]['type'] = predicate_name
                        if predicate_name == 'relation':
                            if 'related' not in data[source]:
                                data[source]['related'] = []
                            if objet not in data[source]['related']:
                                data[source]['related'].append(objet)
                    else:
                        predicate_name = common.get_predicate_name(p)
                        if predicate_name in ['CreationDate', 'ModifiedDate', 'AcceptedDate']:
                            if type(o.value) == datetime:
                                data[source][predicate_name] = o.value
                            elif type(o.value) == date:
                                dt = datetime.combine(o.value, datetime.min.time())
                                data[source][predicate_name] = dt
                            elif type(o.value) == str:
                                try:
                                    data[source][predicate_name] = datetime.strptime(o.value, '%Y-%m-%d')
                                except ValueError:
                                    data[source][predicate_name] = ''
                            else:
                                print(predicate_name, o.value, type(o.value))
                        elif predicate_name in ['Name', 'Date']:
                            if predicate_name not in data[source]:
                                data[source][predicate_name] = []
                            data[source][predicate_name].append(o.value)
                        else:
                            if predicate_name not in data[source]:
                                data[source][predicate_name] = []
                            if common.is_URIRef(o) and objet not in data[source][predicate_name]:
                                data[source][predicate_name].append(objet)
                            language = ''
                            if hasattr(o, 'language') and o.language is not None:
                                language = o.language
                            try:
                                value = o.value
                                if (language, value) not in data[source][predicate_name] and common.is_literal(o):
                                    data[source][predicate_name].append((language, value))
                            except Exception as e:
                                pass
        for entry in data:
            if 'type' in data[entry]:
                if data[entry]['type'] == 'Thesis':
                    subjects = []
                    if 'Subject' in data[entry]:
                        for (lang, subject) in data[entry]['Subject']:
                            if lang == 'fr' or lang == '':
                                subjects.append(subject)
                    good_abstract = ''
                    if 'Abstract' in data[entry]:
                        for (lang, abstract) in data[entry]['Abstract']:
                            if lang == 'fr' or lang == '':
                                good_abstract = abstract
                    good_title = ''
                    for (lang, title) in data[entry]['Title']:
                        if lang == 'fr' or lang == '':
                            good_title = title
                    common.collection_productions.update_one(
                        {'_id': thesis['_id']},
                        {
                            '$set': {
                                'Title': good_title,
                                'Abstract': good_abstract,
                                'Subjects': ", ".join(subjects),
                                'Year': data[entry]['Date'][0],
                                'processing_step': 15
                            }
                        }
                    )
    except SAXParseException:
        pass

