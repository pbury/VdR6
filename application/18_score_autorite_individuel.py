from Common import Common

"""
Compte le nombre de garants dans une these et cherche si une personne est garante de quoi
"""

common = Common()
common.collection_noyau_people.delete_one({'_id': 'Ppn'})

theses_noyau_dur = []
for doc in common.collection_noyau_theses.find({'noyau_dur': True}):
    theses_noyau_dur.append(doc['_id'])

for people in common.collection_noyau_people.find({'is_garant': True}):
    nb_garants = 0
    people_id = people['_id']
    print(people_id)
    if int(people['data']['sudoc']['result']['countRoles']) != 0:
        if int(people['data']['sudoc']['result']['countRoles']) == 1:
            nb_garants += common.count_garants(people['data']['sudoc']['result']['role'], theses_noyau_dur)
        else:
            for role in people['data']['sudoc']['result']['role']:
                nb_garants += common.count_garants(role, theses_noyau_dur)
        common.collection_noyau_people.update_one(
            {'_id': people_id},
            {
                '$set': {
                    'nb_rapporteur_directeur': nb_garants
                }
            }
        )

for people in common.collection_noyau_people.find({'is_garant': True}):
    print(people_id)
    if int(people['data']['sudoc']['result']['countRoles']) != 0:
        score_autorite = float(common.config['scores']['coeff_jury']) * people['nb_rapporteur_directeur']
        score_autorite += float(common.config['scores']['coeff_comites']) * people['nb_comites']
        if 'site_edu_obs' in people:
            score_autorite += float(common.config['scores']['coeff_edu_obs']) * people['site_edu_obs']
        common.collection_noyau_people.update_one(
                {'_id': people['_id']},
                {
                    '$set': {
                        'score_autorite': score_autorite
                    }
                }
            )

