import re
from Common import Common

"""
Analyse le code html d'une thèse afin d'extraire les memebres de jury, les directeurs de thèse, les président de jury ainsi que les rapporteurs
"""

common = Common()
pattern_directeur = re.compile(r'.*Sous la direction de (.*?)rel(.*)')
pattern_rapporteurs = re.compile(r'.*?Les rapporteurs(.*?)div.*')
pattern_president = re.compile(r'.*?sident du jury (.*?)rel.*')
pattern_jures = re.compile(r'.*?Le jury (.*?)div.*')

for thesis in common.collection_noyau_theses.find({'noyau_dur': True}):
    jures = []
    print(thesis['_id'])
    if 'html' in thesis:
        parser = common.MyHTMLParser(common, thesis['_id'])
        parser.feed(str(thesis['html']))

        html = str(thesis['html']).replace("\\r", " ").replace("\\n", "")

        # Directeur de thèse
        match_directeur = pattern_directeur.match(html)
        found_directeur = match_directeur.group(1).strip().replace('<a href="', '').replace('"', "")
        directeur_id = found_directeur
        common.collection_noyau_people.update_one(
            {'_id': directeur_id},
            {
                '$set': {
                    'is_garant': True
                }
            }
        )
        common.collection_noyau_theses.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'people_directeur': directeur_id
                }
            }
        )
        jures.append(directeur_id)

        # rapporteurs
        match_rapporteurs = pattern_rapporteurs.match(html)
        if match_rapporteurs is not None:
            rapporteurs = re.findall(r'http://www.idref.fr/(\d+)/id', (match_rapporteurs.group(1)))


            jures.extend(rapporteurs)
            for rapporteur in rapporteurs:
                common.collection_noyau_people.update_one(
                    {'_id': rapporteur},
                    {
                        '$set': {
                            'is_garant': True
                        }
                    }
                )
            common.collection_noyau_theses.update_one(
                {'_id': thesis['_id']},
                {
                    '$set': {
                        'people_rapporteur': rapporteurs
                    }
                }
            )

        # Président de jury
        match_president = pattern_president.match(html)
        if match_president is not None:
            found_president = match_president.group(1).strip().replace('\\xc3\\xa9tait  <a href="/fr/', '').replace('"', "").strip()
            president_id = found_president
            common.collection_noyau_theses.update_one(
                {'_id': thesis['_id']},
                {
                    '$set': {
                        'people_president': president_id
                    }
                }
            )
            jures.append(president_id)

        # jures
        match_jures = pattern_jures.match(html)
        if match_jures is not None:
            jures_ids = list(set(re.findall(r'http://www.idref.fr/(\d+)/id', (match_jures.group(1)))))
            jures.extend(jures_ids)
            common.collection_noyau_theses.update_one(
                {'_id': thesis['_id']},
                {
                    '$set': {
                        'people_jures': jures_ids
                    }
                }
            )

        # global
        common.collection_noyau_theses.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'jures': list(set(jures)),
                    'nb_jures': len(list(set(jures)))
                }
            }
        )
    else:
        common.collection_noyau_theses.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'noyau_dur': False
                }
            }
        )
