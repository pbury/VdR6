import xlsxwriter

from Common import Common

common = Common()
# default
# seuil_autorite = 0.5
# seuil_predictions = 0.9
seuil_autorite = 1
seuil_predictions = 0.6


columns = {
    'DFP': 'B',
    'EDM': 'C',
    'IOX': 'D',
    'LAN': 'E',
    'POA': 'F',
    'SCO': 'G',
    'UJI': 'H',

}

stats = {}
for year in range(2010, 2020):
    stats[year] = {
        'DFP': 0,
        'EDM': 0,
        'IOX': 0,
        'LAN': 0,
        'POA': 0,
        'SCO': 0,
        'UJI': 0

    }
    query = {
        '$and': [
            {'is_soutenue': True},
            {'Year': str(year)},
            {'score_autorite_pondere': {'$gt': seuil_autorite}}
        ]
    }
    for thesis in common.collection_productions.find(query):
        for column in columns:
            if thesis['prediction_' + column] > seuil_predictions:
                stats[year][column] += 1

workbook = xlsxwriter.Workbook('output_data/taille_halo_1_6.xlsx')
worksheet = workbook.add_worksheet()

for year in range(2010, 2020):
    worksheet.write('A'+str(year-2008), str(year))
    for column in columns:
        worksheet.write(columns[column] + str(year-2008), stats[year][column])

for column in columns:
    print(column)
    worksheet.write(columns[column]+'1', column)

workbook.close()



