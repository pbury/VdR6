import xlsxwriter

from Common import Common

common = Common()

columns = {
    'DFP': 'B',
    'EDM': 'C',
    'IOX': 'D',
    'LAN': 'E',
    'POA': 'F',
    'SCO': 'G',
    'UJI': 'H'
}

lines = {
    'DFP': '2',
    'EDM': '3',
    'IOX': '4',
    'LAN': '5',
    'POA': '6',
    'SCO': '7',
    'UJI': '8'
}


workbook = xlsxwriter.Workbook('output_data/matrice_autocorrelation.xlsx')
worksheet = workbook.add_worksheet()

for column in columns:
    worksheet.write(columns[column] + '1', column)
for line in lines:
    worksheet.write('A' + lines[line], line)

correlation = {}
for column in columns:
    correlation[column] = {}
    for line in columns:
        correlation[column][line] = 0

i = 1
query = {
    '$and': [
        {'is_soutenue': True},
        {'score_autorite_pondere': {'$gte': 0.5}},
        {'$or': [
            {'prediction_SCO': {'$gte': 0.9}},
            {'prediction_UJI': {'$gte': 0.9}},
            {'prediction_DFP': {'$gte': 0.9}},
            {'prediction_POA': {'$gte': 0.9}},
            {'prediction_LAN': {'$gte': 0.9}},
            {'prediction_PDM': {'$gte': 0.9}},
            {'prediction_IOX': {'$gte': 0.9}}]},
        {'$or': [
            {'Year': '2010'},
            {'Year': '2011'},
            {'Year': '2012'},
            {'Year': '2013'},
            {'Year': '2014'},
            {'Year': '2015'},
            {'Year': '2016'},
            {'Year': '2017'},
            {'Year': '2018'},
            {'Year': '2019'},
            {'Year': '2020'}
        ]}
    ]}

for doc in common.collection_productions.find(query):
    i += 1
    print(doc['_id'])
    values = {}
    for column in columns:
        prediction_name = 'prediction_' + column
        if doc[prediction_name] >= 0.9:
            values[column] = True
    for category in values:
        for sub in values:
            correlation[category][sub] += 1
for column in columns:
    for line in lines:
        worksheet.write(columns[column]+lines[line], correlation[column][line])
print(correlation)

workbook.close()



