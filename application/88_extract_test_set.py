import xlsxwriter

from Common import Common

common = Common()

workbook = xlsxwriter.Workbook('output_data/test_set.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'These ID')
worksheet.write('B1', 'Catégories')
worksheet.write('C1', 'Titre')
worksheet.write('D1', 'Résumé')


common = Common()
i = 1
for doc in common.collection_productions.aggregate([
    {'$match': {'is_soutenue': True}},
    {'$sample': {'size': 100}}
]):
    i += 1
    print(doc['_id'])
    worksheet.write('A' + str(i), doc['_id'])
    if 'Title' in doc:
        worksheet.write('C' + str(i), doc['Title'])
    if 'Abstract' in doc:
        worksheet.write('D' + str(i), doc['Abstract'])

workbook.close()



