import re
from Common import Common

"""
Cherche les thèses soutenues parmi la liste des thèses
"""
common = Common()

noyau_dur = []

for doc in common.collection_productions.find({}):
    thesis_id = doc['_id']
    is_soutenue = False
    if doc['noyau_dur'] is False:
        if not re.match('s.*', thesis_id):
            is_soutenue = True
    common.collection_productions.update_one(
        {'_id': thesis_id},
        {
            '$set': {
                'is_soutenue': is_soutenue,
                'processing_step': 13
            }
        }
    )