from random import gauss
import matplotlib.pyplot as plt
import matplotlib
import random
import math
from matplotlib.pyplot import figure

matplotlib.use('TkAgg')

def dessine(is_white, filename, nb):
    point_size = 2
    nb_point_ellipses = 500

    fig, ax = plt.subplots()
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(20, 20)

    ax.add_artist(plt.Circle((0, 0), 100, color='k', fill=False))
    if not is_white:
        ax.add_artist(plt.Circle((0, 0), 200, color='k', fill=False))


    x_LAN = [gauss(-75, 30) for i in range(10*nb['lan'])]
    y_LAN = [gauss(0, 30) for i in range(10*nb['lan'])]
    ax.add_artist(plt.scatter(x_LAN, y_LAN, s=point_size, c='darksalmon'))
    ax.add_artist(plt.text(-75, 0, str(nb['lan']) + ' LAN', fontsize=16, weight='bold'))

    x_POA = [gauss(0, 30) for i in range(10*nb['poa'])]
    y_POA = [gauss(-75, 30) for i in range(10*nb['poa'])]
    ax.add_artist(plt.scatter(x_POA, y_POA, s=point_size, c='mediumorchid'))
    ax.add_artist(plt.text(0, -75, str(nb['poa']) + ' POA', fontsize=16, weight='bold'))

    x_DFP = [gauss(0, 30) for i in range(10*nb['dfp'])]
    y_DFP = [gauss(75, 30) for i in range(10*nb['dfp'])]
    ax.add_artist(plt.scatter(x_DFP, y_DFP, s=point_size, c='tan'))
    ax.add_artist(plt.text(0,75, str(nb['dfp']) + ' DFP', fontsize=16, weight='bold'))

    x_UJI = [gauss(-75, 30) for i in range(10*nb['uji'])]
    y_UJI = [gauss(75, 30) for i in range(10*nb['uji'])]
    ax.add_artist(plt.scatter(x_UJI, y_UJI, s=point_size, c='violet'))
    ax.add_artist(plt.text(-75, 40, str(nb['uji']) + ' UJI', fontsize=16, weight='bold'))

    x_EDM = [gauss(-75, 30) for i in range(10*nb['edm'])]
    y_EDM = [gauss(-75, 30) for i in range(10*nb['edm'])]
    ax.add_artist(plt.scatter(x_EDM, y_EDM, s=point_size, c='lightblue'))
    ax.add_artist(plt.text(-75, -60, str(nb['edm'])+' EDM', fontsize=16, weight='bold'))

    x_IOX = [gauss(75, 30) for i in range(10 * nb['iox'])]
    y_IOX = [gauss(-75, 30) for i in range(10*nb['iox'])]
    ax.add_artist(plt.scatter(x_IOX, y_IOX, s=point_size, c='seagreen'))
    ax.add_artist(plt.text(55, -40, str(nb['iox']) + ' IOX', fontsize=16, weight='bold'))

    x_SCO = [gauss(75, 30) for i in range(10 * nb['sco'])]
    y_SCO = [gauss(75, 30) for i in range(10 * nb['sco'])]
    ax.add_artist(plt.scatter(x_SCO, y_SCO, s=point_size, c='coral'))
    ax.add_artist(plt.text(40, 40, str(nb['sco']) + ' SCO', fontsize=16, weight='bold'))

    if is_white:
        ax.add_artist(plt.Circle((0, 0), 150, color='white', fill=False, linewidth=275))

    plt.title('')
    plt.xlabel('x')
    plt.ylabel('y')

    plt.savefig(filename, dpi=100)
    plt.show()


nb = {
    'lan': 29,
    'poa': 41,
    'sco': 33,
    'iox': 62,
    'edm': 11,
    'dfp': 88,
    'uji': 33

}
dessine(True, 'output_data/fig1', nb)
nb = {
    'lan': 688,
    'poa': 439,
    'sco': 567,
    'iox': 436,
    'edm': 62,
    'dfp': 1127,
    'uji': 321

}
dessine(False, 'output_data/fig2', nb)
# plt.savefig('tt.png')