import xlsxwriter

from Common import Common

common = Common()
workbook = xlsxwriter.Workbook('output_data/noyau_dur.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'These ID')
worksheet.write('B1', 'Année')
worksheet.write('C1', 'Included')
worksheet.write('D1', 'Excluded')
worksheet.write('E1', 'Title Lemmatized')
worksheet.write('F1', 'Abstract Lemmatized')
worksheet.write('G1', 'Subject Lemmatized')
worksheet.write('H1', 'inclusion_éducation_adulte')
worksheet.write('I1', 'inclusion_formation_adulte')
worksheet.write('J1', 'inclusion_éducation_permanent')
worksheet.write('K1', 'inclusion_formation_long_vie')
worksheet.write('L1', 'inclusion_validation_acquis')
worksheet.write('M1', 'inclusion_vae')
worksheet.write('N1', 'inclusion_didactique_professionnel')
worksheet.write('O1', 'inclusion_échange_savoir')
worksheet.write('P1', 'inclusion_apprenance')
worksheet.write('Q1', 'score autorite brut jury')
worksheet.write('R1', 'score autorite pondéré jury')


common = Common()
i = 1
for doc in common.collection_noyau_theses.find({'Abstract_lemmatized': {'$exists': True}}):
    i += 1
    print(doc['thesis_id'])
    worksheet.write('A' + str(i), doc['thesis_id'])
    worksheet.write('B' + str(i), doc['Year'])
    worksheet.write('C' + str(i), doc['included'])
    worksheet.write('D' + str(i), doc['excluded'])
    worksheet.write('E' + str(i), doc['Title_lemmatized'])
    worksheet.write('F' + str(i), doc['Abstract_lemmatized'])
    worksheet.write('G' + str(i), doc['Subjects_lemmatized'])
    worksheet.write('H' + str(i), doc['inclusion_éducation_adulte'])
    worksheet.write('I' + str(i), doc['inclusion_formation_adulte'])
    worksheet.write('J' + str(i), doc['inclusion_éducation_permanent'])
    worksheet.write('K' + str(i), doc['inclusion_formation_long_vie'])
    worksheet.write('L' + str(i), doc['inclusion_validation_acquis'])
    worksheet.write('M' + str(i), doc['inclusion_vae'])
    worksheet.write('N' + str(i), doc['inclusion_didactique_professionnel'])
    worksheet.write('O' + str(i), doc['inclusion_échange_savoir'])
    worksheet.write('P' + str(i), doc['inclusion_apprenance'])
    if 'score_autorite_brut' in doc:
        worksheet.write('Q' + str(i), doc['score_autorite_brut'])
    else:
        worksheet.write('Q' + str(i), 0)
    if 'score_autorite_pondere' in doc:
        worksheet.write('R' + str(i), doc['score_autorite_pondere'])
    else:
        worksheet.write('R' + str(i), 0)

workbook.close()



