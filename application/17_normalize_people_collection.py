from Common import Common

common = Common()

common.collection_noyau_people.update_many(
    {'nb_comites': {'$exists': False}},
    {
        '$set': {
            'nb_comites': 0
        }
    }
)
common.collection_noyau_people.update_many(
    {'Education Permanente': False},
    {
        '$set': {
            'Education Permanente': 0
        }
    }
)
common.collection_noyau_people.update_many(
    {'Savoirs': False},
    {
        '$set': {
            'Savoirs': 0
        }
    }
)
common.collection_noyau_people.update_many(
    {'TransFormations': False},
    {
        '$set': {
            'TransFormations': 0
        }
    }
)
common.collection_noyau_people.update_many(
    {'site_edu_obs': False},
    {
        '$set': {
            'site_edu_obs': 0
        }
    }
)