from Common import Common

"""
Pour chaque thèse créé le jeu des garants
"""

common = Common()

thesis_noyau_dur = []
for doc in common.collection_noyau_theses.find({'included': True}):
    if 'thesis_id' in doc:
        thesis_noyau_dur.append(doc['thesis_id'])

thesis_initital_query = []
for doc in common.collection_noyau_theses.find({}):
    if 'thesis_id' in doc:
        thesis_initital_query.append(doc['thesis_id'])


for doc in common.collection_noyau_people.find({'processing_step': 11}):
    is_jury = False
    nb = {}
    if 'data' in doc and int(doc['data']['sudoc']['result']['countRoles']) != 0:
        if int(doc['data']['sudoc']['result']['countRoles']) == 1:
            role = doc['data']['sudoc']['result']['role']
            is_garant = common.is_role_a_garants_one(role)
            is_jury = common.is_role_in_these_jury_one(role)
            nb[role['unimarcCode']] = role['count']
            if is_jury:
                common.extract_thesis(role, doc['_id'], thesis_noyau_dur, thesis_initital_query)
        else:
            for role in doc['data']['sudoc']['result']['role']:
                is_garant = common.is_role_a_garants_one(role)
                is_jury = is_jury or common.is_role_in_these_jury_one(role)
                nb[role['unimarcCode']] = role['count']
                if is_jury:
                    common.extract_thesis(role, doc['_id'], thesis_noyau_dur, thesis_initital_query)
    common.collection_noyau_people.update_one(
        {'_id': doc['_id']},
        {
            '$set': {
                'is_jury': is_jury,
                'roles': nb,
                'processing_step': 12
            }
        }
    )
