import spacy
from TextProcessing import TextProcessing
from Common import Common

"""
Applique les prétraitement standards (tokenization, lemmatization, stopwords) aux 3 textes des thèses et calcule les sacs de mots (bow)
On traite les résumés, titres et mots-clefs
"""

# import spacy and update stopwords
nlp = spacy.load("fr_core_news_md")
TextProcessing.update_spacy_stopword(
    nlp,
    ("",),
    ("",),
)
common = Common()

for doc in common.collection_noyau_theses.find({'processing_step': 3}):
    print(doc['_id'])
    processor = TextProcessing(
        nlp,
        ("--t::z",)
    )
    processor.clear_lemmatized_text()
    processor.set_text(doc['Abstract'])
    processor.preprocess_one_text()
    processor.tokenize_and_lemnatize()
    lemmatized_text = processor.get_lemmatized_text()
    common.collection_noyau_theses.update_one(
        {'_id': doc['_id']},
        {
            '$set': {
                'Abstract_lemmatized': " ".join(lemmatized_text),
                'Abstract_bow': lemmatized_text,
                'processing_step': 4
            }
        }
    )

    processor.clear_lemmatized_text()
    processor.set_text(doc['Title'])
    processor.preprocess_one_text()
    processor.tokenize_and_lemnatize()
    lemmatized_text = processor.get_lemmatized_text()
    common.collection_noyau_theses.update_one(
        {'_id': doc['_id']},
        {
            '$set': {
                'Title_lemmatized': " ".join(lemmatized_text),
                'Title_bow': lemmatized_text,
                'processing_step': 4
            }
        }
    )
    processor.clear_lemmatized_text()
    processor.set_text(doc['Subjects'])
    processor.preprocess_one_text()
    processor.tokenize_and_lemnatize()
    lemmatized_text = processor.get_lemmatized_text()
    common.collection_noyau_theses.update_one(
        {'_id': doc['_id']},
        {
            '$set': {
                'Subjects_lemmatized': " ".join(lemmatized_text),
                'Subjects_bow': lemmatized_text,
                'processing_step': 4
            }
        }
    )
