import xlsxwriter

from Common import Common

common = Common()

workbook = xlsxwriter.Workbook('output_data/theses_du_noyau2.xlsx')
worksheet = workbook.add_worksheet()

column = {
    'DFP': 'I',
    'EDM': 'J',
    'IOX': 'K',
    'LAN': 'L',
    'POA': 'M',
    'SCO': 'N',
    'UJI': 'O',

}
worksheet.write('A1', 'These ID')
worksheet.write('B1', 'Year')
worksheet.write('C1', 'Auteur')
worksheet.write('D1', 'Directeur de thèse')
worksheet.write('E1', 'Titre')
worksheet.write('F1', 'Résumé')
worksheet.write('G1', 'Catégories')
worksheet.write('H1', 'Sous-catégories')
worksheet.write(column['DFP']+'1', 'DFP')
worksheet.write(column['EDM']+'1', 'EDM')
worksheet.write(column['IOX']+'1', 'IOX')
worksheet.write(column['LAN']+'1', 'LAN')
worksheet.write(column['POA']+'1', 'POA')
worksheet.write(column['SCO']+'1', 'SCO')
worksheet.write(column['UJI']+'1', 'UJI')


common = Common()
i = 1
for doc in common.collection_noyau_theses.find({'included': True}):
    i += 1
    print(doc['thesis_id'])
    worksheet.write('A' + str(i), doc['thesis_id'])
    worksheet.write('B' + str(i), doc['Year'])
    if 'author_name' in doc:
        worksheet.write('C' + str(i), doc['author_name'])
    worksheet.write('E' + str(i), doc['Title'])
    worksheet.write('F' + str(i), doc['Abstract'])
    if 'people_directeur' in doc:
        res = common.collection_noyau_people.find_one({'_id': doc['people_directeur']})
        worksheet.write('D' + str(i), res['name'])
    if 'categories' in doc:
        worksheet.write('G' + str(i), ",".join(doc['categories']))
        for category in doc['categories']:
            worksheet.write(column[category] + str(i), 1)
    if 'sub_categories' in doc:
        worksheet.write('H' + str(i), doc['sub_categories'])

workbook.close()



