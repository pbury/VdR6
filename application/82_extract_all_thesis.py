import xlsxwriter

from Common import Common

common = Common()

workbook = xlsxwriter.Workbook('output_data/theses_to_classify.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'These ID')
worksheet.write('B1', 'Annee')
worksheet.write('C1', 'Ontologie')
worksheet.write('D1', 'Texte')


common = Common()
i = 1
for doc in common.collection_productions.find({'$and': [{'is_soutenue': True}, {'noyau_dur': False}]}):
    i += 1
    print(doc['_id'])
    txt = doc['Title_lemmatized'] + doc['Abstract_lemmatized'] + doc['Subjects_lemmatized']
    worksheet.write('A' + str(i), doc['_id'])
    worksheet.write('B' + str(i), doc['Year'])
    worksheet.write('C' + str(i), '')
    worksheet.write('D' + str(i), txt)

workbook.close()



