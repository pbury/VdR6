import requests
from Common import Common

"""
Telecharge les theses de la table production (la partie html)
"""
common = Common()

while common.collection_productions.find({'$and': [{'processing_step': {'$lt': 21}}, {'is_soutenue': True}]}).count() != 0:
    nb_processed = 0
    to_store = {}
    for thesis in common.collection_productions.find({'$and': [{'processing_step': {'$lt': 21}}, {'is_soutenue': True}]}).limit(20):
        thesis_id = thesis['_id']
        nb_processed += 1
        print("processing %s : %s " % (nb_processed, thesis_id))

        uri = 'https://www.theses.fr/' + thesis_id
        response = requests.get(uri)

        html = ''
        if response.status_code == 200:
            html = response.content
            to_store[thesis_id] = html
        else:
            print('Bad status code for html')
    for thesis_id in to_store:
        common.collection_productions.update_one(
            {'_id': thesis_id},
            {
                '$set': {
                    'html': to_store[thesis_id],
                    'processing_step': 21
                }
            },
            upsert=True
        )

