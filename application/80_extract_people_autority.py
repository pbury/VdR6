import xlsxwriter

from Common import Common

common = Common()
workbook = xlsxwriter.Workbook('output_data/people.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'People ID')
worksheet.write('B1', 'Name')
worksheet.write('C1', 'Score autorité')

common = Common()
i = 1
for doc in common.collection_noyau_people.find({'is_garant': True}):
    i += 1
    worksheet.write('A' + str(i), doc['_id'])
    worksheet.write('B' + str(i), doc['name'])
    if 'score_autorite' in doc:
        worksheet.write('C' + str(i), doc['score_autorite'])
    else:
        worksheet.write('C' + str(i), 0)

workbook.close()




