import csv
from Common import Common

"""
Injecte la liste des membres des comités éditoriaux. Ces personnes sont, par définition, des garants
"""

common = Common()
with open('input_data/edu_obs.csv', newline='') as csvfile:
    file_reader = csv.reader(csvfile, delimiter=';')
    for row in file_reader:
        name = row[0]
        source = row[1]
        ppn = row[2]

        comite_educ = 0
        comite_transfo = 0
        comite_savoirs = 0
        edu_obs = 0
        if source == 'Education Permanente':
            comite_educ = 1
        if source == 'TransFormations':
            comite_transfo = 1
        if source == 'savoirs':
            comite_savoirs = 1
        if source == 'EduObs':
            edu_obs = 1
        if ppn != 'non trouv\u00e9':
            print(ppn)
            common.collection_noyau_people.update_one(
                {'_id': ppn},
                {
                    '$set': {
                        '_id': ppn,
                        'name': name,
                        'processing_step': 9,
                        'is_garant': True
                    }
                },
                upsert=True
            )
            common.collection_noyau_people.update_one(
                {'_id': ppn},
                {
                    '$inc': {
                        'Education Permanente': comite_educ,
                        'TransFormations': comite_transfo,
                        'Savoirs': comite_savoirs,
                        'site_edu_obs': edu_obs,
                    }
                }
            )
            if comite_educ == 1 or comite_transfo == 1 or comite_savoirs == 1:
                common.collection_noyau_people.update_one(
                    {'_id': ppn},
                    {
                        '$inc': {
                            'nb_comites': 1,
                        }
                    }
                )

common.collection_noyau_people.update_many(
    {'nb_comites': {
        '$exists': False
    }},
    {
        '$set': {
            'nb_comites': 0,
        }
    }
)
