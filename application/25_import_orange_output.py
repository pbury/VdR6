from Common import Common
from openpyxl import load_workbook

"""
Importe les prédictions isssues d'Orange
"""

common = Common()
wb = load_workbook(filename='input_data/orange_output.xlsx')
for sheet_name in ('SCO', 'UJI', 'DFP', 'POA', 'LAN', 'EDM', 'IOX'):
    print(sheet_name)
    sheet_ranges = wb[sheet_name]
    for i in range(2, 5705):
        common.collection_productions.update_one(
            {'_id': sheet_ranges['A' + str(i)].value},
            {
                '$set': {
                    'prediction_' + sheet_name: sheet_ranges['C' + str(i)].value
                }
            }
        )
        if i % 100 == 0:
            print(i)
