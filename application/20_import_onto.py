from Common import Common
from openpyxl import load_workbook

"""
Importe les categories d'ontologie des theses du noyau dur
"""

common = Common()
wb = load_workbook(filename='input_data/noyau_dur_CatOnto_pbu.xlsx')
sheet_ranges = wb['Sheet2']
forme_excell = {
   'OVX': 'G',
   'DVP': 'H',
   'DIP': 'I',
   'SAN': 'J',
   'FDF': 'K',
   'LAN': 'L',
   'TEC': 'M',
   'PUN': 'N',
   'ANT': 'O',
   'ECO': 'P',
   'FDM': 'Q',
   'IDM': 'R',
   'INS': 'S',
   'JEU': 'T',
   'MAN': 'U',
   'PDM': 'V',
   'POL': 'W',
   'PSC': 'X',
   'EPO': 'Y',
   'EVP': 'Z',
   'TSO': 'AA',
   'FSC': 'AB',
   'OVP': 'AC',
   'AGRI': 'AD',
   'FEST': 'AE',
}

for i in range(2, 177):
    these_id = sheet_ranges['A'+str(i)].value
    print(these_id)
    subcats = []

    for subcat in forme_excell:
       if sheet_ranges[forme_excell[subcat]+str(i)].value == 1:
          subcats.append(subcat)

    common.collection_noyau_theses.update_one(
       {'thesis_id': these_id},
       {
          '$set': {
             'OVX': sheet_ranges['G'+str(i)].value,
             'DVP': sheet_ranges['H'+str(i)].value,
             'DIP': sheet_ranges['I'+str(i)].value,
             'SAN': sheet_ranges['J'+str(i)].value,
             'FDF': sheet_ranges['K'+str(i)].value,
             'LAN': sheet_ranges['L'+str(i)].value,
             'TEC': sheet_ranges['M'+str(i)].value,
             'PUN': sheet_ranges['N'+str(i)].value,
             'ANT': sheet_ranges['O'+str(i)].value,
             'ECO': sheet_ranges['P'+str(i)].value,
             'FDM': sheet_ranges['Q'+str(i)].value,
             'IDM': sheet_ranges['R'+str(i)].value,
             'INS': sheet_ranges['S'+str(i)].value,
             'JEU': sheet_ranges['T'+str(i)].value,
             'MAN': sheet_ranges['U'+str(i)].value,
             'PDM': sheet_ranges['V'+str(i)].value,
             'POL': sheet_ranges['W'+str(i)].value,
             'PSC': sheet_ranges['X' + str(i)].value,
             'EPO': sheet_ranges['Y' + str(i)].value,
             'EVP': sheet_ranges['Z' + str(i)].value,
             'TSO': sheet_ranges['AA' + str(i)].value,
             'FSC': sheet_ranges['AB' + str(i)].value,
             'OVP': sheet_ranges['AC' + str(i)].value,
             'AGRI': sheet_ranges['AD' + str(i)].value,
             'FEST': sheet_ranges['AE' + str(i)].value,
             'sub_categories': ",".join(subcats)
          }
       }
    )

common.collection_noyau_theses.update_many(
   {},
   {
      '$set': {
         'IOX': False,
         'EDM': False,
         'SCO': False,
         'UJI': False,
         'DFP': False,
         'POA': False,
      }
   }
)

cat = 'IOX'
sub_cat = 'OVX'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'IDM'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'FEST'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'OVP'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
common.collection_noyau_theses.update_many(
   {},
   {
      '$unset': {
         'OVX': True,
         'IDM': True,
         'FEST': True,
         'OVP': True,
      }
   }
)

cat = 'EDM'
sub_cat = 'ECO'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'DRO'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'MAN'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
common.collection_noyau_theses.update_many(
   {},
   {
      '$unset': {
         'ECO': True,
         'DRO': True,
         'MAN': True,
      }
   }
)

cat = 'SCO'
sub_cat = 'PSC'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'FDM'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'FSC'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'PDM'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
common.collection_noyau_theses.update_many(
   {},
   {
      '$unset': {
         'PSC': True,
         'FDM': True,
         'FSC': True,
         'PDM': True,
      }
   }
)

cat = 'UJI'
sub_cat = 'PUN'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'INS'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'JEU'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
common.collection_noyau_theses.update_many(
   {},
   {
      '$unset': {
         'PUN': True,
         'INS': True,
         'JEU': True,
      }
   }
)

cat = 'DFP'
sub_cat = 'TEC'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'DIP'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'FDF'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'DVP'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'EVP'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
common.collection_noyau_theses.update_many(
   {},
   {
      '$unset': {
         'TEC': True,
         'DIP': True,
         'FDF': True,
         'DVP': True,
         'EVP': True,
      }
   }
)

cat = 'POA'
sub_cat = 'ANT'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
sub_cat = 'POL'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
common.collection_noyau_theses.update_many(
   {},
   {
      '$unset': {
         'ANT': True,
         'POL': True,
      }
   }
)

cat = 'LAN'
sub_cat = 'LAN'
common.update_categorie(common.collection_noyau_theses, sub_cat, cat)
common.collection_noyau_theses.update_many(
   {'LAN': 0},
   {
      '$set': {
         'LAN': False,
      }
   }
)

common.collection_noyau_theses.update_many(
   {},
   {
      '$unset': {
         'sub_catagories': True
      }
   }
)