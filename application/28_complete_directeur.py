import re
from Common import Common

"""
Importe les noms de directeurs manquants
"""


common = Common()
pattern_directeur = re.compile(r'.*Sous la direction de (.*?)a>(.*)')

for thesis in common.collection_productions.find({'directeur_name': {'$exists': False}}):
    jures = []
    print(thesis['_id'])
    if 'html' in thesis:
        html = str(thesis['html']).replace("\\r", " ").replace("\\n", "")

        # Directeur de thèse
        match_directeur = pattern_directeur.match(html)
        directeur_name = re.sub('.*?name">', '', match_directeur.group(1))
        directeur_name = re.sub('<.*', '', directeur_name)
        common.collection_productions.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'directeur_name': directeur_name
                }
            }
        )
