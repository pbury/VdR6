import re
from Common import Common

"""
Extrait les noms des auteurs de la partie production
"""

common = Common()
pattern_auteur = re.compile(r'.*<meta name="citation_author" content="(.*?)"(.*)')
collection_noyau = common.collection_noyau_theses.find({})
collection_production = common.collection_productions.find({'$and': [{'is_soutenue': True}, {'author_name': {'$exists': False}}]})
for thesis in collection_production:
    jures = []
    print(thesis['_id'])
    if 'html' in thesis:

        html = str(thesis['html']).replace("\\r", " ").replace("\\n", "")

        # Auteur de thèse
        match_auteur = pattern_auteur.match(html)
        if match_auteur is not None:
            found_auteur = match_auteur.group(1).strip()
            # if thesis['_id'] == '240589173':
            #     print(found_auteur.encode('utf8'))
            #     exit()
            common.collection_productions.update_one(
                {'_id': thesis['_id']},
                {
                    '$set': {
                        'author_name': found_auteur
                    }
                }
            )
