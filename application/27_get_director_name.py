from Common import Common

"""
Importe le nom du directeur de these
"""

common = Common()
collection_production = common.collection_productions.find({'$and': [{'is_soutenue': True}, {'directeur_name': {'$exists': False}}]})
for thesis in collection_production:
    jures = []
    print(thesis['_id'])
    res = common.collection_noyau_people.find_one({'_id': thesis['people_directeur']})

    if res and 'name' in res:
        common.collection_productions.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'directeur_name': res['name']
                }
            }
        )
    else:
        print('not found')
