import re
from Common import Common
from Common import MyHTMLParser

"""
Analyse le code html des theses de la partie production pour en extraire les id de directeur, rapporteurs, président du jury et membres du jury
"""

common = Common()
pattern_directeur = re.compile(r'.*Sous la direction de (.*?)rel(.*)')
pattern_rapporteurs = re.compile(r'.*?Les rapporteurs(.*?)div.*')
pattern_president = re.compile(r'.*?sident du jury (.*?)rel.*')
pattern_jures = re.compile(r'.*?Le jury (.*?)div.*')

for thesis in common.collection_productions.find({'processing_step': 21}):
    jures = []
    print(thesis['_id'])
    if 'html' in thesis:
        parser = MyHTMLParser(common, thesis['_id'])
        parser.feed(str(thesis['html']))

        html = str(thesis['html']).replace("\\r", " ").replace("\\n", "")

        # Directeur de thèse
        match_directeur = pattern_directeur.match(html)
        found_directeur = match_directeur.group(1).strip().replace('<a href="', '').replace('"', "")
        directeur_id = found_directeur
        common.collection_productions.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'people_directeur': directeur_id
                }
            }
        )
        jures.append(directeur_id)

        # rapporteurs
        match_rapporteurs = pattern_rapporteurs.match(html)
        if match_rapporteurs is not None:
            rapporteurs = re.findall(r'http://www.idref.fr/(\d+)/id', (match_rapporteurs.group(1)))


            jures.extend(rapporteurs)
            common.collection_productions.update_one(
                {'_id': thesis['_id']},
                {
                    '$set': {
                        'people_rapporteur': rapporteurs
                    }
                }
            )

        # Président de jury
        match_president = pattern_president.match(html)
        if match_president is not None:
            found_president = match_president.group(1).strip().replace('\\xc3\\xa9tait  <a href="/fr/', '').replace('"', "").strip()
            president_id = found_president
            common.collection_productions.update_one(
                {'_id': thesis['_id']},
                {
                    '$set': {
                        'people_president': president_id
                    }
                }
            )
            jures.append(president_id)

        # jures
        match_jures = pattern_jures.match(html)
        if match_jures is not None:
            jures_ids = list(set(re.findall(r'http://www.idref.fr/(\d+)/id', (match_jures.group(1)))))
            jures.extend(jures_ids)
            common.collection_productions.update_one(
                {'_id': thesis['_id']},
                {
                    '$set': {
                        'people_jures': jures_ids
                    }
                }
            )

        # global
        common.collection_productions.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'jures': list(set(jures)),
                    'nb_jures': len(list(set(jures)))
                }
            }
        )
