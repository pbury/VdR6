import xlsxwriter

from Common import Common

common = Common()

histo_jures = {}
histo_garants = {}
heat = {}
for doc in common.collection_noyau_theses.find({'included': True}):
    if doc['nb_jures'] not in histo_jures:
        histo_jures[doc['nb_jures']] = 1
    else:
        histo_jures[doc['nb_jures']] += 1
    if doc['nb_jures_garants'] not in histo_garants:
        histo_garants[doc['nb_jures_garants']] = 1
    else:
        histo_garants[doc['nb_jures_garants']] += 1



