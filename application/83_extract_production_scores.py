import xlsxwriter

from Common import Common

common = Common()

workbook = xlsxwriter.Workbook('output_data/scores_production.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'These ID')
worksheet.write('B1', 'Score brut')
worksheet.write('C1', 'Score pondéré')
worksheet.write('D1', 'Nb jurés')
worksheet.write('E1', 'Nb jurés garants')


common = Common()
i = 1
for doc in common.collection_productions.find({'processing_step': 23}):
    i += 1
    print(doc['_id'])
    worksheet.write('A' + str(i), doc['_id'])
    worksheet.write('B' + str(i), doc['score_autorite_brut'])
    worksheet.write('C' + str(i), doc['score_autorite_pondere'])
    worksheet.write('D' + str(i), doc['nb_jures'])
    worksheet.write('E' + str(i), doc['nb_jures_garants'])

workbook.close()



