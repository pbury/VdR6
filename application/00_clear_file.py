import re
from Common import Common

""" 
Lit le fichier des thèses telles que récuprées par les requêtes de création du noyau
"""

def clean_line(line):
    return line.strip()


common = Common()

nb_single = 0
nb_ok = 0
nb_without_sudoc = 0
with open("input_data/all.txt", 'r') as fh_in:
    with open('thesis/headers.txt', 'w') as fh_out:
        lines = fh_in.readlines()
        found_thesis = False
        for line in lines:
            cleaned_line = clean_line(line)
            if len(cleaned_line) != 0:
                if re.match("Identifiant pérenne de la notice :", cleaned_line) and \
                        not re.match("dans un autre système", cleaned_line):
                    if found_thesis:
                        nb_single += 1
                    sudoc_id = cleaned_line.replace("Identifiant pérenne de la notice :", "").\
                        replace('http://www.sudoc.fr/', '').strip()
                    common.collection_noyau_theses.update_one(
                        {'_id': sudoc_id},
                        {
                            '$set': {
                                'sudoc_id': sudoc_id,
                                'processing_step': 0
                            }
                        },
                        upsert=True
                    )
                    found_thesis = sudoc_id

                if re.match('Num. national de thèse :', cleaned_line):
                    thesis_id = cleaned_line.replace("Num. national de thèse : ", "").strip()
                    if found_thesis:
                        nb_ok += 1
                        found_thesis = False
                        common.collection_noyau_theses.update_one(
                            {'_id': sudoc_id},
                            {
                                '$set': {
                                    'thesis_id': thesis_id,
                                    'processing_step': 0
                                }
                            },
                            upsert=True
                        )
                    else:
                        nb_without_sudoc += 1
                        print('thesis without sudoc', sudoc_id, thesis_id)
print(nb_single, nb_without_sudoc, nb_ok)



