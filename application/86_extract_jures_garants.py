import xlsxwriter
import matplotlib.pyplot as plt
from Common import Common

common = Common()


workbook = xlsxwriter.Workbook('output_data/nb_jures_garants.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write('A1', 'These ID')
worksheet.write('B1', 'Nb jures')
worksheet.write('C1', 'Nb garants')
worksheet.write('D1', 'Ratio garants/jures')


common = Common()
i = 1
data = []
for doc in common.collection_noyau_theses.find({'included': True}):
    i += 1
    if doc['nb_jures_garants'] != 0:
        worksheet.write('A' + str(i), doc['thesis_id'])
        worksheet.write('B' + str(i), doc['nb_jures'])
        worksheet.write('C' + str(i), doc['nb_jures_garants'])
        worksheet.write('D' + str(i), int((100.0*doc['nb_jures_garants'])/doc['nb_jures']))
        data.append(int(100*doc['nb_jures_garants']/doc['nb_jures']))
workbook.close()
print(data)
plt.hist(data, rwidth=4)
plt.title('Répartition du ratio garants/jurés en %', fontsize=10)
plt.savefig("output_data/ratio_jures_garants.png")
plt.show()



