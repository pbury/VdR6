from Common import Common

"""
Selection les thèses du noyau dur à partir des paramètres *included* et *excluded*
"""

common = Common()
for thesis in common.collection_noyau_theses.find({}):
    selected = False
    if 'included' in thesis:
        common.collection_noyau_theses.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'processing_step': 8,
                    'noyau_dur': thesis['included']
                }
            }
        )
    if 'excluded' in thesis:
        common.collection_noyau_theses.update_one(
            {'_id': thesis['_id']},
            {
                '$set': {
                    'processing_step': 8,
                    'noyau_dur': False
                }
            }
        )

