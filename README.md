# Code utilisés pour établir les données utilisées 
Ces codes sont ceux utilisés pour propduire les résultats  présentés dans la rubrique *vie de la recherche* dans les revues *savoirs* et *transformations*.

Les procédures python sont concues pour être utilisées dans l'ordre, pour les précédures numérotées de 00 à 28. A partir de 79, il s'agit de l'extraction des données dans différentes fichiers excel.

## La structure du projet
- application *(la partie utile du projet)*
    - *input_data* : les données utilisées en entrée
    - *output_data* : le dossier dans lequel on exporte les résultats
    - *.py* tous les fichiers python 
    - *noyau_dur.ini*, le fichier de configuration du projet
- data *(Ici est stocké le contenu des bases de données, pour ne pas les garder dans les container)*
- infrastructure *(tout ce qui est nécessaire pour construire les bases de données et autres autils utilisés)
    - docker *(et cela fonctionne sous docker)
        - mongo
        - docker-compose.yml
- Orange_ows : Les fichiers utilisés pour les traitements de type ML à l'aide des librairies Orange
- tables_references : les tables publiées dans l'article 6 des la rubrique *Vie de la Recherche*
        
## Les fichiers du projet : la partie traitement

- *00_clear_file.py* : Lit le fichier des thèses telles que récuprées par les requêtes de création du noyau. Ce code n'est pas utilisé dans la version actuelle du projet, il est conservé pour archive.
- *01_inject_sudoc_id_from_file.py* : Lit la liste des identifiants de thèse retournée par les requetes initales et les insère en BDD
- *02_get_thesis_xml_and_html.py* : Récupère le contenu html et xml des thèses analysées
- *03_parse_xml.py* : Analyse le contenu xml des thèses
- *04_preprocess_text.py* : Applique les prétraitement standards (tokenization, lemmatization, stopwords) aux 3 textes des thèses et calcule les sacs de mots (bow)
- *05_find_keywords.py* : Cherche les theses dont les textes contiennent les mots recherchés cote à cote après lemmatisation
- *06_exclusion_criteria.py* : Exclu certaines thèses basées sur un fichier d'exclusion (établi heutistiquement)
- *08_select_notau_dur.py* : Selection les thèses du noyau dur à partir des paramètres *included* et *excluded*
- *09a_inject_ppn_from_file.py* : Injecte la liste des membres des comités éditoriaux. Ces personnes sont, par définition, des garants
- *09b_parse_html.py* : Analyse le code html d'une thèse afin d'extraire les memebres de jury, les directeurs de thèse, les président de jury ainsi que les rapporteurs
- *10_get_people_data_from_idref.py* : Pour chaque personne trouvée à l'étape précedente, on télécharge sa notice d'autorité à partir d'IdRef
- *11_get_people_name.py* : Extraction des noms de personnes
- *12_select_people_from_autorities.py* : Pour chaque thèse créé le jeu des garants
- *13_detect_ongoing_thesis.py* : Cherche les thèses soutenues parmi la liste des thèses
- *14_get_productions_abtracts.py* : Recupère le contenu xml des thèses de la table production
- *15_parse_xml.py* : Extrait les Années de publication, Titres, mot-clefs et résumés des thèses
- *16_preprocess_production.py* : Pour les champs mot-clefs, titre et résumé des thèse produites applique la tokenization, suppression des stop-words et la lemmatisation
- *18_score_autorite_individuel.py* : Compte le nombre de garants dans une these et cherche si une personne est garante de quoi
- *19_score_autorite_jury.py* : Calcule le score d'autorite d'un jury à partir des scores de ses membres
- *20_import_onto* : Importe les categories d'ontologie des theses du noyau dur
- *21_get_thesis_html.py* : Telecharge les theses de la table production (la partie html)
- *22_parse_html.py* : Analyse le code html des theses de la partie production pour en extraire les id de directeur, rapporteurs, président du jury et membres du jury
- *23_score_autorite_jury_productions.py* : Calcule le score d'auorite des these de la partie productions
- *24_get_author_name.py* : Extrait les noms des auteurs de la partie production
- *25_import_orange_output.py*: Importe les prédictions isssues d'Orange
- *26_impôrt_test_set.py* : importe le jeu de test
- *27_import_director_name.py*: Importe le nom du directeur de these
- *28_complete_directeur.py* : Importe les noms de directeurs manquants

## Les fichiers du projet : les utilitaires
- *noyau_dur.ini* : la configuration des BDD
- *Common.py* : un ensemble de classes et de méthodes partagées dans le projet
- *Marc21.py* : la partie relative aux code Marc21
- *TextProcessing.py* : la partie concernant le traitement NLP des textes
- *99_reset_db.py* : efface la partie people et productions de la BDD

## les fichiers du projet : l'export

Des scripts 79 à 91.







## Licence
L'ensmeble du code utilisé ici est sous licence **CC-BY-NC-SA** à l'exception du dossier *tables_reference* qui est sous licenece **CC-BY-SA**